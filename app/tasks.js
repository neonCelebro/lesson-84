const express = require('express');
const Task = require('../models/Task');
const auth = require('../auth');
const router = express.Router();

const createRouter = () => {

    router.get('/', auth, async (req, res) => {
        const tasks = await Task.find({user: req.user});
        res.send(tasks);
    });
    router.post('/', auth, (req, res) => {
        const taskData = req.body;
        taskData.user = req.user._id;
        const task = new Task(taskData);

        task.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });
    router.put('/:id', auth, async (req, res) => {
        const taskData = req.body;
        const tasks = await Task.findOne({user: req.user_id});
        if (!tasks) return res.sendStatus(403);

        Task.where({user: req.user._id}, req.params.id).update({
            title: taskData.title ? taskData.title : tasks.title,
            description: taskData.description ? taskData.description : tasks.description,
            status: taskData.status ? taskData.status : tasks.status
        })
            .exec()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.delete('/:id', auth,async (req, res) => {
        const task = await Task.findOne({user: req.user._id, _id: req.params.id});
        if (task){
           await task.remove();
           return res.send("task was deleted")
        } else {
            res.sendStatus(403);
        }
    });
    return router;
};

module.exports = createRouter;
